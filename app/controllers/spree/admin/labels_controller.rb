class Spree::Admin::LabelsController < Spree::Admin::BaseController
    require 'savon'
    require 'net/http'
    require 'digest/md5'
    before_action :load_order
    
    def show
        @label = Spree::Label.find(params[:id])
    end
    
    def new
        if Spree::Label.exists?(order_id: @order.id)
            @label = Spree::Label.find_by(order_id: @order.id)
        else
            @label = @order.build_label
        end
    end
    
    def create
        @label = @order.build_label(label_params)
        @label.Security = (Digest::MD5.hexdigest(@label.concat_label)).upcase
        client = Savon.client(wsdl: "http://api.mondialrelay.com/Web_Services.asmx?WSDL", logger: Rails.logger,
                              log: true, pretty_print_xml: true)
    
    response = client.call(:wsi2_creation_etiquette, message: { "Enseigne" => @label.Enseigne,
                                    "ModeCol" => @label.ModeCol,
                                    "ModeLiv" => @label.ModeLiv,
                                    "NDossier" => @label.NDossier,
                                    "Expe_Langage" => @label.Expe_Langage,
                                    "Expe_Ad1" => @label.Expe_Ad1,
                                    "Expe_Ad3" => @label.Expe_Ad3,
                                    "Expe_Ad4" => @label.Expe_Ad4,
                                    "Expe_Ville" => @label.Expe_Ville,
                                    "Expe_CP" => @label.Expe_CP,
                                    "Expe_Pays" => @label.Expe_Pays,
                                    "Expe_Tel1" => @label.Expe_Tel1,
                                    "Expe_Mail" => @label.Expe_Mail,
                                    "Dest_Langage" => @label.Dest_Langage,
                                    "Dest_Ad1" => @label.Dest_Ad1,
                                    "Dest_Ad3" => @label.Dest_Ad3,
                                    "Dest_Ad4" => @label.Dest_Ad4,
                                    "Dest_Ville" => @label.Dest_Ville,
                                    "Dest_CP" => @label.Dest_CP,
                                    "Dest_Pays" => @label.Dest_Pays,
                                    "Dest_Tel1" => @label.Dest_Tel1,
                                    "Dest_Mail" => @label.Dest_Mail,
                                    "Poids" => @label.weight,
                                    "NbColis" => @label.NbColis,
                                    "CRT_Valeur" => @label.CRT_Valeur,
                                    "COL_Rel_Pays" => @label.COL_Rel_Pays,
                                    "COL_Rel" => @label.COL_Rel,
                                    "LIV_Rel_Pays" => @label.LIV_Rel_Pays,
                                    "LIV_Rel" => @label.LIV_Rel,
                                    "Security" => @label.Security
                                    })
                                    
    if response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:stat] == "0"
        @label.stat = response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:stat]
        @label.expeditionnum = response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:expedition_num]
        @label.url_etiquette = "http://www.mondialrelay.com" + response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:url_etiquette]
        if @label.save
            redirect_to admin_order_label_path(@order, @label)
        else
            render 'new'
        end
    else
        @label.update_attributes(stat: response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:stat])
        render 'new'
    end
    
    end
    
    def update
        @label = @order.build_label(label_params)
        @label.Security = (Digest::MD5.hexdigest(@label.concat_label)).upcase
        client = Savon.client(wsdl: "http://api.mondialrelay.com/Web_Services.asmx?WSDL", logger: Rails.logger,
                              log: true, pretty_print_xml: true)
    
    response = client.call(:wsi2_creation_etiquette, message: { "Enseigne" => @label.Enseigne,
                                    "ModeCol" => @label.ModeCol,
                                    "ModeLiv" => @label.ModeLiv,
                                    "NDossier" => @label.NDossier,
                                    "Expe_Langage" => @label.Expe_Langage,
                                    "Expe_Ad1" => @label.Expe_Ad1,
                                    "Expe_Ad3" => @label.Expe_Ad3,
                                    "Expe_Ad4" => @label.Expe_Ad4,
                                    "Expe_Ville" => @label.Expe_Ville,
                                    "Expe_CP" => @label.Expe_CP,
                                    "Expe_Pays" => @label.Expe_Pays,
                                    "Expe_Tel1" => @label.Expe_Tel1,
                                    "Expe_Mail" => @label.Expe_Mail,
                                    "Dest_Langage" => @label.Dest_Langage,
                                    "Dest_Ad1" => @label.Dest_Ad1,
                                    "Dest_Ad3" => @label.Dest_Ad3,
                                    "Dest_Ad4" => @label.Dest_Ad4,
                                    "Dest_Ville" => @label.Dest_Ville,
                                    "Dest_CP" => @label.Dest_CP,
                                    "Dest_Pays" => @label.Dest_Pays,
                                    "Dest_Tel1" => @label.Dest_Tel1,
                                    "Dest_Mail" => @label.Dest_Mail,
                                    "Poids" => @label.weight,
                                    "NbColis" => @label.NbColis,
                                    "CRT_Valeur" => @label.CRT_Valeur,
                                    "COL_Rel_Pays" => @label.COL_Rel_Pays,
                                    "COL_Rel" => @label.COL_Rel,
                                    "LIV_Rel_Pays" => @label.LIV_Rel_Pays,
                                    "LIV_Rel" => @label.LIV_Rel,
                                    "Security" => @label.Security
                                    })
                                    
    if response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:stat] == "0"
        @label.stat = response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:stat]
        @label.expeditionnum = response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:expedition_num]
        @label.url_etiquette = "http://www.mondialrelay.com" + response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:url_etiquette]
        if @label.save
            redirect_to admin_order_label_path(@order, @label)
        else
            render 'new'
        end
    else
        @label.update_attributes(stat: response.body[:wsi2_creation_etiquette_response][:wsi2_creation_etiquette_result][:stat])
        render 'new'
    end

    end
    
        private
        
        def load_order
            @order = Spree::Order.friendly.find(params[:order_id])
        end
        
        def label_params
            params.require(:label).permit(:Enseigne, :ModeCol, :ModeLiv, :NDossier, :Expe_Langage, :Expe_Ad1, :Expe_Ad3, :Expe_Ad4,
                  :Expe_Ville, :Expe_CP, :Expe_Pays, :Expe_Tel1, :Expe_Mail, :Dest_Langage, :Dest_Ad1,
                  :Dest_Ad3, :Dest_Ad4, :Dest_Ville, :Dest_CP, :Dest_Pays, :Dest_Tel1, :Dest_Mail, :NbColis,
                  :CRT_Valeur, :COL_Rel_Pays, :COL_Rel, :LIV_Rel_Pays, :LIV_Rel, :weight, :key)
        end
        
        
        
        
        
        
        
     
    
end