Spree::CheckoutController.class_eval do
    
    private
    
    def before_delivery
      return if params[:order].present?

      packages = @order.shipments.map(&:to_package)
      @differentiator = Spree::Stock::Differentiator.new(@order, packages)
      if Spree::Relai.where(order_id: @order.id).empty?
        @relais = @order.relais.build
      else
        @relais = @order.relais.last
      end
    end
    
    
end