Deface::Override.new(:virtual_path => "spree/admin/shared/_order_tabs",
                     :name => "add_label",
                     :insert_after => "[data-hook='admin_order_tabs_state_changes']",
                     :partial => "spree/shared/label")