Spree::Order.class_eval do
    has_many :relais, dependent: :destroy
    has_one :label, dependent: :destroy
    accepts_nested_attributes_for :relais
end