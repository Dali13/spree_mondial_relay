class Spree::Label < ActiveRecord::Base
    belongs_to :order
    attr_accessor :Enseigne, :ModeCol, :ModeLiv, :NDossier, :Expe_Langage, :Expe_Ad1, :Expe_Ad3, :Expe_Ad4,
                  :Expe_Ville, :Expe_CP, :Expe_Pays, :Expe_Tel1, :Expe_Mail, :Dest_Langage, :Dest_Ad1,
                  :Dest_Ad3, :Dest_Ad4, :Dest_Ville, :Dest_CP, :Dest_Pays, :Dest_Tel1, :Dest_Mail, :NbColis,
                  :CRT_Valeur, :COL_Rel_Pays, :COL_Rel, :LIV_Rel_Pays, :LIV_Rel, :Security, :key
                  
        def concat_label
            "#{self.Enseigne}" + "#{self.ModeCol}" + "#{self.ModeLiv}" + "#{self.NDossier}" +  "#{self.Expe_Langage}" +  
            "#{self.Expe_Ad1}" + "#{self.Expe_Ad3}" + "#{self.Expe_Ad4}" + "#{self.Expe_Ville}" + "#{self.Expe_CP}" + "#{self.Expe_Pays}" + 
            "#{self.Expe_Tel1}" + "#{self.Expe_Mail}" + "#{self.Dest_Langage}" + "#{self.Dest_Ad1}" + "#{self.Dest_Ad3}" + "#{self.Dest_Ad4}" +
            "#{self.Dest_Ville}" + "#{self.Dest_CP}" + "#{self.Dest_Pays}" + "#{self.Dest_Tel1}" + "#{self.Dest_Mail}" + "#{self.weight}" +
            "#{self.NbColis}" + "#{self.CRT_Valeur}" + "#{self.COL_Rel_Pays}" +  "#{self.COL_Rel}" + "#{self.LIV_Rel_Pays}" + "#{self.LIV_Rel}" +
            "#{self.key}"
        end
end
