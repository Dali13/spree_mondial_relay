class CreateSpreeRelais < ActiveRecord::Migration
  def change
    create_table :spree_relais do |t|
      t.string :number
      t.string :name
      t.string :address
      t.string :cp
      t.string :city
      t.string :country
      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
