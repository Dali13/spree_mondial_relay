class AddRelayToSpreeOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :relay, :boolean, default: false
  end
end
