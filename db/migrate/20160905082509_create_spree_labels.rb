class CreateSpreeLabels < ActiveRecord::Migration
  def change
    create_table :spree_labels do |t|
      t.integer :weight
      t.string :stat
      t.string :expeditionnum
      t.text :url_etiquette
      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
