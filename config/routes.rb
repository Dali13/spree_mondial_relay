Spree::Core::Engine.routes.draw do
  namespace :admin, path: Spree.admin_path do
      resources :orders, except: [:show] do
          resources :labels, only: [:new, :create, :show, :update]
      end
  end
end
